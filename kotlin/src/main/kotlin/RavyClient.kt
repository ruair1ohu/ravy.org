import org.jetbrains.annotations.Nullable

class RavyClient(private var token: String, @Nullable ksoft: Boolean) {
    var type: String;

    init {
        type = if (ksoft) "Ksoft" else "Ravy"
    }

    fun getToken(): String {
        return "$type $token";
    }
}