# Ravy.org Trust and Moderation API

This library is a JavaScript HTTP client for interacting with [Ravy](https://ravy.pink)'s moderation and trust API (https://ravy.org/api).

This package's source code is included in a monorepository containing code for packages in other languages. Visit [the repository](https://ravy.dev/Splatterxl/ravy.org) for more information.

## Getting started

Initialising the client is easy.

```js
import RavyClient from "ravy.org";

const client = new RavyClient("my token");
```

If you are migrating from [KSoft.Si](https://ksoft.si), you may set the second parameter to `true`.

```js 
const client = new RavyClient("my ksoft token", true);
```

See the [documentation](https://ravy.dev/Splatterxl/ravy.org/docs/README.md) for more examples and advanced documentation.

## Links
- [Ravy.org API Documentation](https://ravy.org/api)
- [Similar libraries](https://ravy.dev/Splatterxl/ravy.org/)