import { RavyClient } from "../src/index.js";
import dotenv from "dotenv";
import util from "util";
import assert from "assert";

dotenv.config();

describe("Client", () => {
    it("should error when no token is provided", () => {
      try {
        new RavyClient();
        throw new Error("Didn't error");
      } catch {
        // great! it errored
      }
    })

  const client = new RavyClient(process.env.RAVY_TOKEN);

  describe("#token()", () => {
    let data;
    it("shouldn't error with a valid token", (done) => {
      client.token().then(res => {
        data = res;
        done();
      });
    });
    it("shouldn't enumerate the token secret", () => {
      if (util.inspect(data).includes("secret")) {
        throw new Error("Structure shows 'secret' key in util.inspect()")
      }
    })
  })

  describe("#url()", () => {
    let fraudulent = "https://discrod.gift";
    let ok = "https://discord.com";

    it("should return 'true' if the URL is fraudulent", async () => {
      assert(client.url(fraudulent), "Fraudulent URL is flagged");
    });

    it("should return 'false' if the URL is not flagged", async () => {
      assert(client.url(ok), "False positive for https://discord.com")
    })
  })
})
