import req from "@aero/http";
import RavyClient from "./client.js";

/**
 * @interface {import("@aero/centra").Request} Request
 * @interface {import("@aero/centra").Response} Response
 */

export class HTTPClient {
  static baseUrl = "https://ravy.org/api/v1"

  #token;

  /**
   * @param {RavyClient} client
   */
  constructor(client) {
    // type will be provided
    this.#token = client.getToken();
    this.client = client;
  }

  /**
   * @internal
   * @param {string} url
   * @param {URLSearchParams} query
   */
  async get(url, query = new URLSearchParams()) {
    const request = req(HTTPClient.baseUrl + url, "GET")
        .header("Authorization", this.#token)
        .header("Accept", "application/json")
        .agent(this.client.getUserAgent());

    // aero http doesnt support URLSearchParams
    query.forEach((value, key) => request.query(key, value));

    return await this.send(request);
  }

  /**
   * @internal
   * @param {string} url
   * @param {any} data
   * @param {URLSearchParams} query
   */
  async post(url, data, query = new URLSearchParams()) {
    const request = req(HTTPClient.baseUrl + url, "POST")
        .header("Authorization", this.#token)
        .header("Accept", "application/json")
        .header("Content-Type", "application/json")
        .agent(this.client.getUserAgent())
        .body(data, "json");

    // aero http doesnt support URLSearchParams
    query.forEach((value, key) => request.query(key, value));

    return await this.send(request);
  }

  /** @param {req.Request} req */
  async send(req) {
    console.debug(`[ravy.org] ${req.httpMethod} ${req.url}`);
    return await this.handle(await req.send(), req);
  }

  /**
   * @param {req.ResponseData} resp
   * @param {string} url
   * @param {string} method
   * @param {req.Request} req
   */ 
  async handle(res, req) {
    if (res.statusCode < 300) {
      // everything OK 
      return res.body.json();
    } else if (res.statusCode < 500) {
      // client error  
      error(res.statusCode, req.httpMethod, req.url, await res.body.json());
    } else {
      throw new Error(`[ravy.org] ${res.statusCode} @ ${req.method} ${req.url}`);
    }
  }
}

function error(code, method, url, data) {
  throw new Error(`[ravy.org] ${method} ${url}: ${code} [${data.error} — ${data.message}]`);
}
