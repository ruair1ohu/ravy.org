import { HTTPClient } from './http.js';
import { TokenInfo } from './structures/TokenInfo.js';
import { URLData } from './structures/URLData.js';
import { User } from './structures/User.js';

/**
 * The base client for interacting with Ravy.org.
 */
export class RavyClient {
  #token;
  /**
   * @param {string} token Your ravy.org token.
   * @param {ClientOptions} [options]
   */
  constructor(token, options = {}) {
    if (!token) throw new Error('[Ravy.org] Please provide an API token.');
    this.#token = token;
    this.type = options.ksoft ? 'Ksoft' : 'Ravy';
    this.http = new HTTPClient(this);

    this.setUserAgent(
      options.userAgent ||
        `Ravy.org-JS (+https://ravy.dev/Splatterxl/ravy-api) Node.js/${process.version} (+https://nodejs.org) Aero-HTTP`
    );
    if (options.phisherman) this.setPhishermanOptions(options.phisherman);
  }

  /**
   * @internal
   * @returns {string}
   */
  getToken() {
    return `${this.type} ${this.#token}`;
  }

  /**
   * Add a custom user agent to the HTTP client.
   * @param {string} userAgent
   */
  setUserAgent(userAgent) {
    this.userAgent = userAgent;
  }

  /**
   * @internal
   * @returns {string}
   */
  getUserAgent() {
    return this.http.userAgent;
  }

  /**
   * Add a Phisherman API token to the client.
   * @param {string} token
   * @param {string} user
   */
  setPhishermanOptions(token, user) {
    this.phisherman = {
      token,
      user,
    };
  }

  /**
   * @internal
   */
  hasPhisherman() {
    return this.phisherman && this.phisherman.token && this.phisherman.user;
  }

  /**
   * @internal
   */
  getPhishermanOptions() {
    return this.phisherman;
  }

  /**
   * Returns information about your Ravy.org token.
   * @returns {TokenInfo}
   */
  async token() {
    return new TokenInfo(await this.http.get('/tokens/@current'));
  }

  /**
   * Queries a URL's legitimacy using various sources. If you have a Phisherman API token, it will use that.
   * @param {string} url
   * @param {string} [author] Discord user ID of the message author, for auto-banning.
   * @returns {URLData}
   */
  async url(url, author = null) {
    const urlData = new URL(url);

    const domain = `${urlData.protocol}//${urlData.hostname}/`;
    const query = new URLSearchParams();

    if (author) query.append('author', author);
    if (this.hasPhisherman()) {
      query.append('phisherman_token', this.getPhishermanOptions().token);
      query.append('phisherman_user', this.getPhishermanOptions().user);
    }

    return this.http
      .get('/urls/' + encodeURIComponent(domain), query)
      .then(d => new URLData(d));
  }

  /**
   * Returns extensive user information, including trust, whitelists, bans, reputation and sentinel data.
   * @param {string} id
   * @returns {User}
   */
  async user(id) {
    if (!id) throw new Error('[Ravy.org] Please provide a user ID.');
    return this.http
      .get('/users/' + encodeURIComponent(id))
      .then(d => new User(d));
  }

  /**
   * Returns the user's pronouns.
   * @param {string} id
   * @returns {string}
   */
  async userPronouns(id) {
    if (!id) throw new Error('[Ravy.org] Please provide a user ID.');
    return this.http
      .get('/users/' + encodeURIComponent(id) + '/pronouns')
      .then(d => d.pronouns);
  }

  /**
   * Returns the user's bans from various sources.
   * @param {string} id
   * @returns {UserBanInfo}
   */
  async userBans(id) {
    if (!id) throw new Error('[Ravy.org] Please provide a user ID.');
    return this.http.get('/users/' + encodeURIComponent(id) + '/bans');
  }

  /**
   * Add a ban to the user's ban list.
   * @param {string} id
   * @param {UserBan & { reason: string }} ban
   * @returns {never}
   */
  async userAddBan(id, ban) {
    if (!id) throw new Error('[Ravy.org] Please provide a user ID.');
    if (!ban) throw new Error('[Ravy.org] Please provide a ban.');
    if (!ban.reason) throw new Error('[Ravy.org] Please provide a ban reason.');
    return this.http.post('/users/' + encodeURIComponent(id) + '/bans', ban);
  }
}

/**
 * Options for the {@link RavyClient} constructor.
 * @typedef {{
 *  ksoft?: boolean,
 *  userAgent?: string,
 *  phisherman?: {
 *    token: string,
 *    user: string
 *  }
 * }} ClientOptions
 */

export default RavyClient;
