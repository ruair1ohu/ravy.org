export class TokenInfo {
  /** 
   * The user ID the token has been granted to.
   * @type {string}
   */ 
  user;

  /**
   * The access nodes the token has access to.
   * @type {string[]}
   */ 
  access;

  /**
   * Application identifier for the token.
   * @type {string}
   */ 
  application;

  /**
   * Whether this token is migrated from KSoft.SI or a Ravy.org native token.
   * @type {"ravy" | "ksoft"}
   */ 
  type;

  constructor(data) {
    Object.assign(this, data);
  }
}
