const IsFraudulent = Symbol('IsFraudulent');

export class URLData {
  [IsFraudulent];
  message = "";

  constructor(data) {
    this.message = data.message;
    this[IsFraudulent] = data.isFraudulent;
  }

  get fraudulent() {
    return this[IsFraudulent];
  }
}
