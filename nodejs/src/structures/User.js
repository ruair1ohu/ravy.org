export class User {
  /**
   * The preferred pronouns for this user.
   * @type {string}
   */
  pronouns;

  /**
   * Any bans associated with the user.
   * @type {UserBanInfo[]}
   */
  bans = [];

  /**
   * The trust level associated with the user.
   * @type {{ level: number, label: string }}
   */
  trust = { level: 3, label: 'no data' };

  /**
   * The whitelists associated with the user.
   *
   * @type {WhitelistInfo[]}
   */
  whitelists = [];

  /**
   * The reputation associated with the user.
   * @type {{ provider: string; score: number; upvotes?: number; downvotes?:number }[]}
   */
  reputation = [];

  /**
   * Aero Sentinel information associated with the user.
   * @type {{ verified: boolean; id?: string }}
   */
  sentinel;

  constructor(user) {
    this.pronouns = user.pronouns;
    this.bans = user.bans;
    this.trust = user.trust;
    this.whitelists = user.whitelists;
    this.reputation = user.reputation;
    this.sentinel = user.sentinel;
  }
}
