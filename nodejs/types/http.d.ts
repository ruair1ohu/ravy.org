/**
 * @interface {import("@aero/centra").Request} Request
 * @interface {import("@aero/centra").Response} Response
 */
export class HTTPClient {
    static baseUrl: string;
    /**
     * @param {RavyClient} client
     */
    constructor(client: RavyClient);
    client: RavyClient;
    /**
     * @internal
     * @param {string} url
     * @param {URLSearchParams} query
     */
    get(url: string, query?: URLSearchParams): Promise<any>;
    /**
     * @internal
     * @param {string} url
     * @param {any} data
     * @param {URLSearchParams} query
     */
    post(url: string, data: any, query?: URLSearchParams): Promise<any>;
    /** @param {req.Request} req */
    send(req: req.Request): Promise<any>;
    /**
     * @param {req.ResponseData} resp
     * @param {string} url
     * @param {string} method
     * @param {req.Request} req
     */
    handle(res: any, req: req.Request): Promise<any>;
    #private;
}
import RavyClient from "./client.js";
import req from "@aero/http";
