export class User {
    constructor(user: any);
    /**
     * The preferred pronouns for this user.
     * @type {string}
     */
    pronouns: string;
    /**
     * Any bans associated with the user.
     * @type {UserBanInfo[]}
     */
    bans: UserBanInfo[];
    /**
     * The trust level associated with the user.
     * @type {{ trust: number, label: string }}
     */
    trust: {
        trust: number;
        label: string;
    };
    /**
     * The whitelists associated with the user.
     *
     * @type {WhitelistInfo[]}
     */
    whitelists: WhitelistInfo[];
    /**
     * The reputation associated with the user.
     * @type {{ provider: string; score: number; upvotes?: number; downvotes?:number }[]}
     */
    reputation: {
        provider: string;
        score: number;
        upvotes?: number;
        downvotes?: number;
    }[];
    /**
     * Aero Sentinel information associated with the user.
     * @type {{ verified: boolean; id?: string }}
     */
    sentinel: {
        verified: boolean;
        id?: string;
    };
}
