export class TokenInfo {
    constructor(data: any);
    /**
     * The user ID the token has been granted to.
     * @type {string}
     */
    user: string;
    /**
     * The access nodes the token has access to.
     * @type {string[]}
     */
    access: string[];
    /**
     * Application identifier for the token.
     * @type {string}
     */
    application: string;
    /**
     * Whether this token is migrated from KSoft.SI or a Ravy.org native token.
     * @type {"ravy" | "ksoft"}
     */
    type: "ravy" | "ksoft";
}
