/**
 * The base client for interacting with Ravy.org.
 */
export class RavyClient {
    /**
     * @param {string} token Your ravy.org token.
     * @param {ClientOptions} [options]
     */
    constructor(token: string, options?: ClientOptions);
    type: string;
    http: HTTPClient;
    /**
     * @internal
     * @returns {string}
     */
    getToken(): string;
    /**
     * Add a custom user agent to the HTTP client.
     * @param {string} userAgent
     */
    setUserAgent(userAgent: string): void;
    userAgent: string;
    /**
     * @internal
     * @returns {string}
     */
    getUserAgent(): string;
    /**
     * Add a Phisherman API token to the client.
     * @param {string} token
     * @param {string} user
     */
    setPhishermanOptions(token: string, user: string): void;
    phisherman: {
        token: string;
        user: string;
    };
    /**
     * @internal
     */
    hasPhisherman(): string;
    /**
     * @internal
     */
    getPhishermanOptions(): {
        token: string;
        user: string;
    };
    /**
     * Returns information about your Ravy.org token.
     * @returns {TokenInfo}
     */
    token(): TokenInfo;
    /**
     * Queries a URL's legitimacy using various sources. If you have a Phisherman API token, it will use that.
     * @param {string} url
     * @param {string} [author] Discord user ID of the message author, for auto-banning.
     * @returns {URLData}
     */
    url(url: string, author?: string): URLData;
    /**
     * Returns extensive user information, including trust, whitelists, bans, reputation and sentinel data.
     * @param {string} id
     * @returns {User}
     */
    user(id: string): User;
    /**
     * Returns the user's pronouns.
     * @param {string} id
     * @returns {string}
     */
    userPronouns(id: string): string;
    /**
     * Returns the user's bans from various sources.
     * @param {string} id
     * @returns {UserBanInfo}
     */
    userBans(id: string): UserBanInfo;
    /**
     * Add a ban to the user's ban list.
     * @param {string} id
     * @param {UserBan & { reason: string }} ban
     * @returns {never}
     */
    userAddBan(id: string, ban: UserBan & {
        reason: string;
    }): never;
    #private;
}
export default RavyClient;
/**
 * Options for the {@link RavyClient } constructor.
 */
export type ClientOptions = {
    ksoft?: boolean;
    userAgent?: string;
    phisherman?: {
        token: string;
        user: string;
    };
};
import { HTTPClient } from "./http.js";
import { TokenInfo } from "./structures/TokenInfo.js";
import { URLData } from "./structures/URLData.js";
import { User } from "./structures/User.js";
