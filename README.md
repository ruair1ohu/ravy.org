# Ravy.org API Libraries

This repository is a collection of libraries for interacting with [Ravy](https://ravy.pink)'s moderation and trust API (<https://ravy.org/api>).

Libraries for different languages are sorted into their respective language's folder.

## Links
- [JavaScript library](./ravy.org-js)
- [Kotlin library](./kotlin)
